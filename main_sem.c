//ADAM SAWICKI
//270814
//Systemy operacyjne - pracownia 2
//Problem producentów i konsumentów
//Wersja semafory

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <stdbool.h>

int liczba = 0;




typedef struct
{
    int buffer_size;
    int * buffer_tab;   //buffer working as stack
    int iter;           //iterator to work on buffer
    sem_t can_produce;      //producer can produce if consumer remove number from buffer
    sem_t can_consume;      //consumer can consume if producer added number to buffer
    sem_t mutex;

} buffer_struct;

typedef struct
{
    buffer_struct * buffer;
    int producer_number;
    pthread_t * producer_threads;
} producer_struct;


typedef struct
{
    buffer_struct * buffer;
    int consumer_number;
    pthread_t * consumer_threads;
} consumer_struct;

void * producer(void *arg)
{
    producer_struct * producers = (producer_struct*)arg;
    buffer_struct * buffer = producers->buffer;
    int id_number;
    pthread_t id = pthread_self();
    for(int i=0; i<producers->producer_number; i++)
    {
        if(pthread_equal(id, producers->producer_threads[i]))
        {
            id_number=i;
            break;
        }
    }
    struct timespec time;

    while(true)
    {

        sem_wait(&buffer->can_produce);
        sem_wait(&buffer->mutex);

        printf("Producer number %d produced: %d\n", id_number, liczba);

        buffer->iter++;
        buffer->buffer_tab[buffer->iter] = liczba;
        liczba++;

        sem_post(&buffer->mutex);

        sem_post(&buffer->can_consume);

        time.tv_nsec = rand()%1000000;
        //nanosleep(&time, NULL);
        sleep(1);
    }
}

void * consumer(void *arg)
{
    consumer_struct * consumers = (consumer_struct*)arg;
    buffer_struct * buffer = consumers->buffer;
    int id_number;
    pthread_t id = pthread_self();
    for(int i=0; i<consumers->consumer_number; i++)
    {
        if(pthread_equal(id, consumers->consumer_threads[i]))
        {
            id_number=i;
            break;
        }
    }


    struct timespec time;

    while(true)
    {

        sem_wait(&buffer->can_consume);
        sem_wait(&buffer->mutex);

        printf("Consumer number %d consumed: %d\n", id_number, buffer->buffer_tab[buffer->iter]);

        buffer->iter--;

        sem_post(&buffer->mutex);

        sem_post(&buffer->can_produce);

        time.tv_nsec = rand()%1000000;
        //nanosleep(&time, NULL);
        sleep(1);
    }
}


int main()
{
    srand(time(NULL));

    //Buffer
    ///////////////////////////
    buffer_struct buffer = {

    .buffer_size = 10,
    .iter=-1,
    .buffer_tab = (int*)malloc(sizeof(int)*buffer.buffer_size)
    };
    sem_init(&buffer.mutex, 0, 1);
    sem_init(&buffer.can_produce, 0 ,buffer.buffer_size);
    sem_init(&buffer.can_consume, 0 ,0);

    //Consumers
    //////////////////////////
    consumer_struct consumers = {

    .consumer_number = 5,

    .buffer = &buffer,

    .consumer_threads = (pthread_t*)malloc(sizeof(pthread_t)*consumers.consumer_number)
    };
    //Producers
    //////////////////////////
    producer_struct producers = {
    .producer_number = 5,

    .buffer = &buffer,

    .producer_threads = (pthread_t*)malloc(sizeof(pthread_t)*producers.producer_number)
    };
    //////////////////////////


    //Threads creation
    for(int i=0; i<producers.producer_number; i++)
    {
        pthread_create(&producers.producer_threads[i], NULL, producer, &producers);
    }

    for(int i=0; i<consumers.consumer_number; i++)
    {
        pthread_create(&consumers.consumer_threads[i], NULL, consumer, &consumers);
    }

    //Joining threads
    for(int i=0; i<producers.producer_number; i++)
    {
        pthread_join(producers.producer_threads[i], NULL);
    }

    for(int i=0; i<consumers.consumer_number; i++)
    {
        pthread_join(consumers.consumer_threads[i], NULL);
    }

    return 0;
}
